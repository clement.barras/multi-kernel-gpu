import errno
import os
import pickle

import numpy as np
import pandas as pd
import torch


# checking

def check_type(o, valid_types, object_name=""):
    if not isinstance(o, valid_types):
        if isinstance(valid_types, tuple):
            message = f"Object {object_name} is type {type(o).__name__}. " \
                      f"However, accepted types are {tuple(t.__name__ for t in valid_types)}."
        else:  # type(valid_types) = type
            message = f"Object {object_name} is type {type(o).__name__}. " \
                      f"However, accepted type is {valid_types.__name__}."
        raise TypeError(message)


def check_positive_definite(ar):
    if not (check_symmetric(ar) and (np.linalg.eigvals(ar) >= -1e-3).all()):
        raise np.linalg.LinAlgError("Kernel matrix not positive semi-definite")


def check_symmetric(a, tol=1e-8):
    return np.all(np.abs(a - a.T) < tol)


### torch and numpy conversions

def np2torch(a, device, req_grad=False):
    return torch.tensor(a, device=device, dtype=torch.float32, requires_grad=req_grad)


def torch2np(t):
    return t.cpu().detach().numpy().astype(np.float32)


### File and directory management
def create_directory(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def fname_wout_extension(path):
    fname = path.split("/")[-1]
    fname = fname.split(".")[0]
    return fname


def get_extension(path):
    return "." + path.split(".")[-1]


def filter_contents(df, column, values):
    return df.loc[df[column].isin(np.ascontiguousarray(values))]


def open_dataframe(object, index_col=None, low_memory=False, parse_dates=False):
    if isinstance(object, pd.DataFrame):
        return object
    elif isinstance(object, str):
        obj_ext = get_extension(object)
        if obj_ext == ".csv":
            df = pd.read_csv(object, index_col=index_col, low_memory=low_memory, parse_dates=parse_dates)
            return df
        else:
            raise ValueError("The file is a {} file. Only csv files can be open.".format(obj_ext))
    else:
        raise ValueError(
            "Object of type {} has been passed while a file path or a Dataframe was expected.".format(type(object)))


## MEM utils ##
def mem_report():
    # https://gist.github.com/Stonesjtu/368ddf5d9eb56669269ecdf9b0d21cbe
    '''Report the memory usage of the tensor.storage in pytorch
    Both on CPUs and GPUs are reported'''

    def _mem_report(tensors, mem_type):
        '''Print the selected tensors of type
        There are two major storage types in our major concern:
            - GPU: tensors transferred to CUDA devices
            - CPU: tensors remaining on the system memory (usually unimportant)
        Args:
            - tensors: the tensors of specified type
            - mem_type: 'CPU' or 'GPU' in current implementation '''
        print('Storage on %s' % (mem_type))
        print('-' * LEN)
        total_numel = 0
        total_mem = 0
        visited_data = []
        for tensor in tensors:
            if tensor.is_sparse:
                continue
            # a data_ptr indicates a memory block allocated
            data_ptr = tensor.storage().data_ptr()
            if data_ptr in visited_data:
                continue
            visited_data.append(data_ptr)

            numel = tensor.storage().size()
            total_numel += numel
            element_size = tensor.storage().element_size()
            mem = numel * element_size / 1024 / 1024  # 32bit=4Byte, MByte
            total_mem += mem
            element_type = type(tensor).__name__
            size = tuple(tensor.size())

            print('%s\t\t%s\t\t%.2f' % (
                element_type,
                size,
                mem))
        print('-' * LEN)
        print('Total Tensors: %d \tUsed Memory Space: %.2f MBytes' % (total_numel, total_mem))
        print('-' * LEN)

    LEN = 65
    print('=' * LEN)
    objects = gc.get_objects()
    print('%s\t%s\t\t\t%s' % ('Element type', 'Size', 'Used MEM(MBytes)'))
    tensors = [obj for obj in objects if torch.is_tensor(obj)]
    cuda_tensors = [t for t in tensors if t.is_cuda]
    host_tensors = [t for t in tensors if not t.is_cuda]
    _mem_report(cuda_tensors, 'GPU')
    _mem_report(host_tensors, 'CPU')
    print('=' * LEN)


# Genome data management

def pack_binary_genome(geno, out_file=None):
    # convert genome csv file to a compact binary representation 8 times smaller)
    if isinstance(geno, str):
        geno = pd.read_csv(geno, index_col=0)
    elif isinstance(geno, pd.DataFrame):
        pass
    else:
        raise ValueError("Genotype should be passed as a csv file name or pandas Dataframe.")
    packed_data = dict()
    packed_data["idx"] = geno.index
    packed_data["col"] = geno.columns
    packed_data["shape"] = geno.shape
    geno_ary = geno.values.astype(np.int8)
    packed_data["max"] = np.max(geno_ary)
    packed_data["min"] = np.min(geno_ary)
    geno_ary -= np.min(geno_ary)
    geno_ary //= np.max(geno_ary)
    packed_data["data"] = np.packbits(geno_ary, axis=0)
    with open(out_file, 'wb') as fp:
        pickle.dump(packed_data, fp, pickle.HIGHEST_PROTOCOL)
    return packed_data


def unpack_binary_genome(packed_file):
    # reverse operation
    if isinstance(packed_file, str):
        with open(packed_file, 'rb') as fp:
            packed_data = pickle.load(fp)
    elif isinstance(packed_file, dict):
        packed_data = packed_file
    else:
        raise ValueError("Packed genotype should be passed as a pickle file or a dictionary.")
    geno_ary = np.unpackbits(packed_data["data"], axis=0).astype(np.float32)
    shp = packed_data["shape"]
    geno_ary = geno_ary[:shp[0], :shp[1]]
    geno_ary *= (packed_data["max"] - packed_data["min"])
    geno_ary += packed_data["min"]
    geno = pd.DataFrame(index=packed_data["col"], columns=packed_data["idx"], data=geno_ary.T)
    return geno
