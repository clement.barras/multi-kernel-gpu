import time
from typing import Union

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import torch

from .kernel import Kernel
from .kernel_calc import prepare_data
from .utils import np2torch, torch2np, check_type


class MultipleKernel:

    def __init__(self, kernels: list, target: pd.DataFrame, pairwise: bool, device: str = "cpu",
                 weights_init_var: float = 0, seed: int = 0,
                 to_norm: bool = True) -> None:

        """

        Parameters
        ----------
        kernels: list of Kernel objects to combine.

        target: DataFrame containing data to predict their corresponding indexes.

        pairwise: determines if all pairwise products between all input kernels should be added automatically or not.

        device: indicates the device to be used by pytorch. Can be "cpu", "cuda" or "cuda:{k}" where k is the GPU ID.

        weights_init_var: variance for weights initialization. If 0, all weights will be set to 0

        seed: seed used for random processes

        to_norm: indicates whether the kernel must be normalized.
        """

        check_type(kernels, list, "kernels")
        check_type(target, pd.DataFrame, "target")
        check_type(pairwise, bool, "pairwise")
        check_type(device, str, "device")
        check_type(to_norm, bool, "to_norm")

        self.target = target
        self.device = device
        self.to_norm = to_norm

        # Create and reindex the kernel list according to target
        self.base_kernels = []  # base kernels, with no pairwise
        for kernel in kernels:
            check_type(kernel, Kernel, kernel)
            if not kernel.is_reindexed:
                kernel.reindex(target)
            self.base_kernels.append(kernel)

        # Include pairwise products or not
        if pairwise:
            self.kernels = []  # includes base kernels and pairwise products of them
            n = len(self.base_kernels)
            for i in range(n):
                kernel1 = self.base_kernels[i]
                self.kernels.append(kernel1)
                for j in range(i, n):
                    kernel2 = self.base_kernels[j]
                    self.kernels.append(self.elementwise_prod(kernel1, kernel2))
        else:
            self.kernels = [k for k in self.base_kernels]

        self._weights = None
        self.weights = None  # self._weight = abs(self._weights)
        self.weights_init_var = weights_init_var
        self.seed = seed
        np.random.seed(self.seed)
        self.kernel_combo = self.linear_combination(self.kernels, self.weights)  # current kernel
        self.optimizer = None  # optimizer and its state
        self.optimizer_state_dict = None
        self.n_steps = 0  # total training steps
        self.y = None  # target to predict
        torch.cuda.empty_cache()

    @property
    def n_kernels(self) -> int:
        return len(self.kernels)

    @property
    def kernel_values(self) -> list:
        return [k.values for k in self.kernels]

    @property
    def kernel_array(self) -> np.ndarray:
        return torch2np(self.kernel_combo.values)

    def append_kernel(self, kernel: Kernel) -> None:
        # Manually add a kernel
        check_type(kernel, Kernel, "kernel")
        if not kernel.is_reindexed:
            kernel.reindex(self.target)
        self.base_kernels.append(kernel)
        self.kernels.append(kernel)

    def elementwise_prod(self, kernel1: Kernel, kernel2: Kernel, typecheck: bool = True) -> Kernel:
        # creates a kernel pairwise (Hadamard) product between two kernels
        if typecheck:
            check_type(kernel1, Kernel, "kernel1")
            check_type(kernel2, Kernel, "kernel2")
        if not kernel1.is_reindexed:
            kernel1.reindex(self.target)
        if not kernel2.is_reindexed:
            kernel2.reindex(self.target)
        values = torch2np(torch.mul(kernel1.values, kernel2.values))
        name = f"{kernel1.name}*{kernel2.name}"
        return self._create_generic_kernel(values, name)

    def linear_combination(self, kernels: list, weights: Union[None, np.ndarray, torch.Tensor] = None,
                           typecheck: bool = True) -> Kernel:
        """

        Parameters
        ----------
        kernels: list of Kernels to combine
        weights: if array or tensor, contains kernel weights. If None, uniform weights will be used.
        typecheck: if check typing of input. May be disabled to increase performance.

        Returns
        -------
        kernel: kernels combination.
        """
        if typecheck:
            check_type(kernels, list, "kernels")
            check_type(weights, (type(None), np.ndarray, torch.Tensor), "weights")
        # convert weights to torch Tensor. Note that gradient is required by the optimization algorithm.
        if isinstance(weights, np.ndarray):
            weights = np2torch(weights, self.device, req_grad=True)
        if weights is None:  # if weights are not specified,
            if self.weights_init_var == 0:
                init_weights = np.ones(self.n_kernels, dtype=np.float32)
            else:
                std = np.sqrt(np.abs(self.weights_init_var))
                init_weights = 1 + std * np.random.rand(self.n_kernels).astype(np.float32)
            weights = np2torch(init_weights / self.n_kernels, self.device, req_grad=True)
        # check that sizes match
        if len(kernels) != len(weights):
            raise ValueError(
                f"Number of kernels ({len(kernels)}) does not match with number of weigths ({len(weights)})")
        # check that all kernels have the same indexation
        for kernel in kernels:
            if typecheck:
                check_type(kernel, Kernel, "kernel")
            if not kernel.is_reindexed:
                kernel.reindex(self.target)
        # make weights positive
        self._weights = weights
        weights = weights.abs()
        # compute the linear combination, using GPU if enabled
        kernels = torch.stack([k.values for k in kernels], dim=0)
        kernels = torch.mul(weights[:, None, None], kernels)
        values = torch2np(torch.sum(kernels, dim=0))
        name = "Combined kernel"
        self.weights = weights  # update weights accordingly
        kernel = self._create_generic_kernel(values, name)
        return kernel

    def _create_generic_kernel(self, values: np.ndarray, name: Union[str, None] = None) -> Kernel:
        """

        Parameters
        ----------
        values: array containing the kernel matrix
        name: kernel name

        Returns
        -------
        kernel: created from the given values and name
        """
        index = np.arange(values.shape[0])
        kern = pd.DataFrame(values, index=index, columns=index)
        kernel = Kernel(kern, None, device=self.device, kernel_name=name, to_norm=self.to_norm)
        kernel.is_indexed = True
        return kernel

    def optimize_weights(self, target_col: str, rows: Union[str, list, np.ndarray] = 'all',
                         n_iters: Union[None, int] = None, n_folds: int = 10,
                         optimizer: Union[None, torch.optim.Optimizer] = None,
                         optim_lr: Union[int, float] = 2e-4, coef_l1: Union[int, float] = 1e-2,
                         coef_l2: Union[int, float] = 1e-2, ridge_l2: Union[int, float] = 0.01,
                         ):
        """

        Parameters
        ----------
        target_col: column of target to predict.

        rows: subset of rows to use for optimizing weights.

        n_iters: number of iterations of optimization.

        n_folds: number of folds for the internal cross-validation process.

        optimizer: per default None, Adam optimizer will be used.

        optim_lr: optimizer learning rate. Lower is slower but more stable.

        coef_l1: l1 penalization coef for the weights.

        coef_l2: l2 penalization coef for the weights.

        ridge_l2: internal l2 coefficient.

        Returns
        -------
        weights: resulting weights array
        """

        check_type(target_col, str, "target_col")
        check_type(n_iters, (type(None), int, float), "n_iters")
        if n_iters is not None and n_iters < 0:
            raise ValueError("n_iters must be positive.")
        check_type(n_folds, int, "n_folds")
        if n_folds < 0:
            raise ValueError("n_folds must be positive.")
        check_type(optimizer, (type(None), torch.optim.Optimizer), "optimizer")
        check_type(optim_lr, (float, int), "optim_lr")
        check_type(coef_l1, (float, int), "coef_l1")
        check_type(coef_l2, (float, int), "coef_l2")
        check_type(ridge_l2, (float, int), "ridge_l2")
        if ridge_l2 < 0 or coef_l1 < 0 or coef_l2 < 0:
            raise ValueError("Regularization parameters must all be positive.")
        check_type(rows, (str, list, np.ndarray), "rows")
        # if not weights are defined, use uniform ones
        if self._weights is None:
            weights = np2torch(np.ones(self.n_kernels, dtype=np.float32) / self.n_kernels, self.device, req_grad=True)
        else:
            weights = self._weights
        # optimizer management
        if optimizer is not None:
            optimizer.param_groups[0]["params"] = [weights]
        else:  # if no optimizer was passed as an argument
            if self.optimizer is None:  # if no existing optimizer (e.g. first optimization)
                optimizer = torch.optim.Adam([weights], lr=optim_lr)
            else:
                self.optimizer.load_state_dict(self.optimizer_state_dict)  # otherwise, load parameters
                optimizer = self.optimizer
                for g in optimizer.param_groups:
                    g['lr'] = optim_lr
        # prepare the target to predict
        y = self.target[target_col].values
        y = prepare_data(y)
        self.y = y.flatten()
        y = np2torch(y, self.device, True)
        # precalculate parameters used for faster function evaluation
        kernel_stack = torch.stack(self.kernel_values, dim=2)  # contains all kernels for easier computation
        if not(isinstance(rows, str) and rows=="all"):
            y = y[rows]
            kernel_stack = kernel_stack[rows][:, rows]
        n = len(y)  # number of samples to predict
        m = n // n_folds  # number of samples per fold
        n_tr = m * (n_folds - 1)  # number of samples used at each step of cross validation
        # defining lists of cross validation indexes
        tr_idxs_list = [np.setdiff1d(range(m * n_folds), range(i * m, (i + 1) * m)) for i in range(n_folds)]
        te_idxs_list = [range(i * m, (i + 1) * m) for i in range(n_folds)]
        tr_idxs_ten = torch.Tensor(np.stack(tr_idxs_list, axis=0)).long().to(self.device)
        te_idxs_ten = torch.Tensor(np.stack(te_idxs_list, axis=0)).long().to(self.device)
        y_tr = torch.stack([y[tr_idxs_ten[i]] for i in range(n_folds)])
        y_te = torch.stack([y[te_idxs_ten[i]] for i in range(n_folds)])
        pen = ridge_l2 * n_tr * torch.eye(n_tr, device=self.device, dtype=torch.float32)  # internal ridge penalization

        def compute_loss(w):
            # given a vector of weights, gives the cross validation prediction loss using a Kernel ridge model
            # see https://www.ics.uci.edu/~welling/classnotes/papers_class/Kernel-Ridge.pdf
            # all calculations are performed by batches of n_folds to accelerate calculations.
            w = w.abs()  # important
            kernel_combo = torch.sum(torch.mul(kernel_stack, w), dim=2)
            k_tr = torch.stack([kernel_combo[tr_idxs_ten[i]][:, tr_idxs_ten[i]] for i in range(n_folds)])
            k_te = torch.stack([kernel_combo[te_idxs_ten[i]][:, tr_idxs_ten[i]] for i in range(n_folds)])
            alpha = torch.bmm((k_tr + pen).inverse(), y_tr)
            y_pred = torch.bmm(k_te, alpha)
            loss = (y_te - y_pred).pow(2).mean()
            mse = loss.item()
            loss = loss + coef_l1 * w.abs().mean() + coef_l2 * w.pow(2).mean().pow(0.5)
            return loss, mse

        def closure():
            # training iteration
            t0 = time.time()
            optimizer.zero_grad()
            loss, rmse = compute_loss(weights)
            print_loss = loss.item()
            loss.backward(retain_graph=True)
            # if not (self.n_steps % 20):
            #     print(
            #         f"Step#{self.n_steps}, Loss: {print_loss:0.3e},"
            #         f" MSE: {rmse:0.3e}, Time elapsed: {time.time() - t0 : 0.3f}s")
            return loss

        # main fitting loop
        n_iters = 1000000 if n_iters is None else int(n_iters)
        try:
            for it in range(n_iters):
                optimizer.zero_grad()
                optimizer.step(closure)
                self.n_steps += 1
        except KeyboardInterrupt:  # if keyboard interrupt ends the process without raising an exception
            pass
        except Exception as e:
            raise e
        self._weights = weights
        self.weights = weights.abs()
        self.kernel_combo = self.linear_combination(self.kernels, self.weights)
        self.optimizer = optimizer
        self.optimizer_state_dict = optimizer.state_dict()
        return self.weights

    def plot_weights(self) -> None:
        heat = torch2np(self.weights)
        heat /= np.sum(heat)
        heat = heat.tolist()
        fig = go.Figure(data=go.Heatmap(z=[heat], x=[k.name for k in self.kernels], y=['Kernel ID']))
        fig.show()
