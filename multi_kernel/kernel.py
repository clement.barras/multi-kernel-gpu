from typing import Union, Sequence

import numpy as np
import pandas as pd
import torch

from .kernel_calc import calc_linear_kernel, calc_gaussian_kernel, tracenorm
from .utils import torch2np, np2torch, check_type, check_positive_definite


class Kernel:
    name_ctr = 0

    def __init__(self, data: Union[pd.DataFrame, str], index_name: Union[None, str] = None,
                 kernel_type: str = "precomputed", device: str = "cpu", kernel_name: Union[None, str] = None,
                 to_norm: bool = True, keep_columns: Union[None, Sequence[str]] = None,
                 kernel_params: Union[None, dict] = None) -> None:
        """

        Parameters
        ----------
        data: DataFrame or csv file containing data related to kernel calculation.

        index_name: column(s) of data used to identify the kernel's rows and columns.

        kernel_type: can be "precomputed" (kernel = data), "linear" (kernel = data.data^T), "RBF_from_dist"
        (kernel = exp(-gamma*data)) or "RBF_from_data" (kernel = exp(-gamma*dist) where dist = distance_matrix(data)).

        device: indicates the device to be used by pytorch. Can be "cpu", "cuda" or "cuda:{k}" where k is the GPU ID.

        kernel_name: name to give to the kernel. If None, will be defined as an Unnamed Kernel.

        to_norm: if the kernel should be normalized or not.

        keep_columns: determines the columns to keep in data.

        kernel_params: dictionary containing kernel parameters. For now, only gamma parameter is supported.
        """
        check_type(data, (pd.DataFrame, str), "data")
        check_type(index_name, (type(None), str), "index_col")
        check_type(kernel_type, str, "kernel_type")
        check_type(device, str, "device")
        check_type(kernel_name, (type(None), str), "kernel_name")
        check_type(to_norm, bool, "to_norm")
        check_type(keep_columns, (type(None), list), "keep_columns")
        check_type(kernel_params, (type(None), dict), "kernel_params")

        # open data file
        if isinstance(data, str):
            data = pd.read_csv(data, index_col=0)
        elif isinstance(data, pd.DataFrame):
            data = data.copy(deep=True)  # copy to prevent side-effects

        # index management
        if index_name is None or index_name == data.index.name:  # if no index, take data index as index
            self.index = data.index
            self.index_name = data.index.name
        else:
            self.index = pd.Index(data[index_name])
            self.index_name = index_name

        # columns filtering
        if keep_columns is None:  # keep all columns
            self.keep_columns = data.columns
        else:
            data = data[keep_columns]  # keep only specified columns
            self.keep_columns = keep_columns

        # kernel calculation
        if kernel_params is None:
            kernel_params = {}
        data = data.astype(np.float32)
        if kernel_type == "precomputed":
            if data.values.shape[0] != data.values.shape[1]:
                raise ValueError(f"Passed matrix is not square (size f{data.values.shape})")
            self.values = np2torch(data.values, device, False)
            check_positive_definite(data.values)
        elif kernel_type == "linear":  # linear kernel : X.X^T
            self.values = calc_linear_kernel(data.values, device, to_norm)
        elif kernel_type in ["RBF_from_dist", "RBF_from_data"]:  # gaussian kernel from distance matrix
            gamma = kernel_params["gamma"] if "gamma" in kernel_params else None
            from_dist = kernel_type == "RBF_from_dist"
            # gamma is determined if not defined
            self.values, self.gamma = calc_gaussian_kernel(data.values, gamma, from_dist, device, to_norm)
        else:
            raise ValueError(f"Kernel type {kernel_type} not recognized")
        self.fill_na()
        self.kernel_type = kernel_type
        self.device = device
        self.kernel_params = kernel_params
        self.to_norm = to_norm
        self.is_norm = self.to_norm
        self.shape = tuple(self.values.shape)

        # determine kernel name
        if kernel_name is None:
            self.name = f"Unnamed kernel #{Kernel.name_ctr}"
            Kernel.name_ctr += 1
        else:
            self.name = kernel_name

        self.is_reindexed = False
        torch.cuda.empty_cache()  # empty GPU cache

    def __repr__(self) -> str:
        # String returned when the kernel is printed.
        np.set_printoptions(edgeitems=7)
        values_str = torch2np(self.values)
        # Set numpy print parameters for better array visualization
        values_str = np.array2string(values_str, max_line_width=120, precision=3, separator=" ")

        return f"""
Kernel object
Name: {self.name}
Device: {self.device}

Shape: {self.shape}
Indexes: {self.index}
Indexing columns: {self.index_name}
Columns: {self.keep_columns}

Kernel type: {self.kernel_type}
Kernel params: {self.kernel_params}
Is normalized: {self.is_norm}
Is indexed: {self.is_reindexed}

Values: 
{values_str}
                
                """

    def norm(self) -> None:
        # Normalize the kernel.
        self.values = tracenorm(self.values)
        self.is_norm = True

    def fill_na(self) -> None:
        if torch.sum(torch.isnan(self.values)):  # if there is at least one missing value
            # all missing values will be replaced by the average of non-missing, non-zero values.
            print(self.values)
            nanmean = torch.mean(self.values[~torch.isnan(self.values) & (self.values!=0)])
            print(nanmean)
            self.values[torch.isnan(self.values)] = nanmean

    def to_device(self, device: str) -> None:  # move to device (can be cpu or cuda)
        self.values = self.values.to(device)

    def reindex(self, indexing_df: [pd.DataFrame]) -> None:
        """
        Reindex the kernel with a new index/multi-indexes defined by a dataframe.
        This can be used to :
        - reorder the kernel's rows and columns
        - select only a subset of rows and columns
        - add new rows and columns to the kernel, the values of which being filled the same way as missing values.
        """
        check_type(indexing_df, pd.DataFrame, "indexes")
        if (self.index_name is None) or (self.index_name == indexing_df.index.name):
            new_index = indexing_df.index
        else:
            new_index = pd.Index(indexing_df[self.index_name].values.flatten())

        # before the final reindexing, a preliminary reindexing is done with an index without repetition.
        # this enables a more consistent and faster filling of missing values
        unique_index = new_index.unique()
        self.values = self._reindex(self.index, unique_index)
        self.fill_na()
        self.values = self._reindex(unique_index, new_index)  # final reindexing
        self.index = new_index
        self.shape = self.values.shape
        self.is_reindexed = True
        self.is_norm = False
        if self.to_norm:
            self.norm()
        torch.cuda.empty_cache()

    def _reindex(self, old_idx: Union[pd.Index, pd.MultiIndex],
                 new_idx: Union[pd.Index, pd.MultiIndex]) -> torch.Tensor:
        # Underlying reindex function reindexing the kernel's values only
        df = pd.DataFrame(torch2np(self.values), index=old_idx, columns=old_idx)
        df = df.reindex(index=new_idx)
        df = df.reindex(columns=new_idx)
        return np2torch(df.values, self.device)
