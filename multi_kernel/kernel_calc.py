import numpy as np
import torch

from .utils import np2torch, check_type


# Normalizations
def prepare_data(d):
    # Prepare observation matrix before calculating kernel
    m = np.asarray(d, dtype=np.float32)  # convert to array
    if m.ndim == 1:  # if 1D array, transform to 2D
        m = m[:, None]
    for j in range(m.shape[1]):  # column wise normalization
        col = m[:, j]
        if np.all(col == col[0]):  # if constant column, pass
            continue
        col -= np.nanmean(col)  # centering
        col[np.isnan(col)] = 0  # fill missing by 0
        col /= np.std(col)  # standardization
        m[:, j] = col
    return m


def tracenorm(k):
    # normalize a kernel such that the average of its diagonal elements becomes 1
    factor = torch.trace(k) / k.shape[0]
    return k / factor


def calc_linear_kernel(data, device="cpu", to_norm=True):
    data = prepare_data(data)  # normalize data, fill missing values
    data = np2torch(data, device)
    k = data.mm(data.t())
    if to_norm:
        return tracenorm(k)
    else:
        return k


def calc_gaussian_kernel(data, gamma=None, from_dist=False, device="cpu", to_norm=True):
    if not from_dist:
        data = prepare_data(data)  # normalize data, fill missing values
        data = np2torch(data, device)
        data = calc_distance_matrix(data, data)
    else:
        data = np2torch(data, device)
    if gamma is None:
        gamma = calc_default_gamma(data)
    else:
        if gamma <= 0:
            raise ValueError(f"gamma = {gamma}. Parameter gamma must be strictly positive.")
    k = torch.exp(-gamma * data)
    if to_norm:
        return tracenorm(k), gamma
    else:
        return k, gamma

def calc_default_gamma(dist):
    gamma = 1 / (1.414 * torch.median(dist[torch.nonzero(dist)]).item())
    return gamma


def calc_distance_matrix(x, y):
    # Calculate the distance matrix between samples of x (size n*d) and y (size m*d)
    # See https://discuss.pytorch.org/t/efficient-distance-matrix-computation/9065/4
    n = x.size(0)
    m = y.size(0)
    d = x.size(1)
    x = x.unsqueeze(1).expand(n, m, d)
    y = y.unsqueeze(0).expand(n, m, d)
    dist = torch.pow(x - y, 2).sum(2)
    return dist
