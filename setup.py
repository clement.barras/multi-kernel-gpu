from distutils.core import setup

setup(name="multi_kernel", version="1.0.3", author="Clement Barras",
      author_email="clementbarras@hotmail.fr",
      package_dir={'multi_kernel': 'multi_kernel'},
      packages=['multi_kernel'],
      install_requires=["numpy", "pandas", "torch", "plotly"])
